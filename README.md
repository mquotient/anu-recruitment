# Problem Statement #
The objective is to create a django project with the following features

* An authentication system where the application allows the user to login and logout.
* Provide a UI to add/remove users
* Provide access control to various modules based on the users rights.  Different users should be allowed or disallowed certain functions.
* Create a Django app as described below
* **Bonus Point** If you are using Django Admin module, then theme the module with something other than the default theme

# Provider App #
The objective is to create an app called `provider`.  The provider will have a model called `Provider` with the following structure
and a UI for CRUD.

## Model ##
```
class Provider(object):
    """
	Object Attributes:
    name - A string field which has limtis on maximum size.  Mandatory field. 
			Should contain only Alphabets, digits and _.  Should begin with an alphabet
	geography - One among South Asia, Africa Europe, North America, South America, 
					Australia
	country - Stores the name of country.  For a given geography the list of valid countries change.
				This may be obtained from https://en.wikipedia.org/wiki/List_of_sovereign_states_and_dependent_territories_by_continent
				You may pick a few countries from each continent
	accounts - A list field containing strings.  
	"""
	pass
	
```

## UI ##
Create an UI (Django Admin is acceptable) to perform CRUD operations on the model.  The following
The UI should have the following features

* The `name` field should implement the validations as per the specification in the model
* `geography` and `country` should be a drop down
* When `geography` selected, the `country` drop down should update.
* Provide a file upload field whose content can be used to update the `accounts` list field

# Instructions #
1. Write well documented object oriented code to achieve the objective
2. The code should adher to pep8 standards as much as possible
3. You may make any reasonable assumptions beyond what is specified
4. You are free to define additional apps, models, views and API for achieving the objective
5. The code must be pushed to this repo.  You may make multiple commits.  When committing 
provide proper commit messages
6. The project root should contain a file "requirements.txt" which should have all the required python libraries which should
be installed for running the project
7. The project root should contain a file "instructions.md" which should contain instructions for setting up and 
running the project