# Instructions to setup project #
	Assumes latest versions of postgres, pgAdmin, and git are all available in your computer.
1. Create a virtual environment for setting up the project.
2. Clone the project using `git clone git@bitbucket.org:mquotient/anu-recruitment.git .` command or if you already have it then update to lastest version using `git pull origin master` command. 
3. After you have the latest version of code, install all required libraries from requirements.txt file using `pip install -r requirements.txt` command.
4. Now create a new login role in pgAdmin with role name `recruitment` and password `recruitment`, then create a new database with name `recruitment` and the role we created as owner. 
5. Now go to project root and create tables using `python manage.py migrate` command.
6. Create superuser using `python manage.py createsuperuser` command.
7. Now populate database using some predefined values by using `python manage.py populatedatabase` command.
8. Run the local server using `python manage.py runserver` command.
9. You will be able to access the admin site via `localhost:8000/admin` by signin using your superuser credentials. You will be able to perform the CRUD operations on models or create other users with varying permissions and rights.
