from django.contrib import admin

from .models import Geography, Country, Provider, Account


# Used for overriding the default save method to update contents of
# account attribute in corresponding Provider class from the uploaded 
# file while saving. Source - 
# https://docs.djangoproject.com/en/dev/ref/contrib/admin/#django.contrib.admin.ModelAdmin.save_model

class AccountAdmin(admin.ModelAdmin):
    
    def save_model(self, request, obj, form, change):
        file = obj.accounts_file
        lines = file.readlines()
        for line in lines:
            obj.provider.account.append(line.replace('\n', ''))
            obj.provider.save()
        super(AccountAdmin, self).save_model(request, obj, form, change)


# Used for displaying contents of each object like a table in
# admin interface.

class ProviderAdmin(admin.ModelAdmin):
    list_display = ('name', 'country', 'account')


admin.site.register(Geography)
admin.site.register(Country)
admin.site.register(Provider, ProviderAdmin)
admin.site.register(Account, AccountAdmin)