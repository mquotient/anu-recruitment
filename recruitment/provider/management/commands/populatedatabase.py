# A custom script used populating the Geography and Country classes
# from some predefined values from REGIONS.

from django.core.management.base import BaseCommand

from provider.models import Geography, Country


def populate_database(regions):
    for region in regions:
        geography, created = Geography.objects.get_or_create(
            name=region)
        geography.save()
        for nation in regions[region]:
            country, created = Country.objects.get_or_create(
                name=nation,
                geography=geography)
            country.save()


class Command(BaseCommand):
    help = 'Populate Database'

    def handle(self, *args, **options):
        REGIONS = {
            'South Asia': ['India', 'Sri Lanka', 'Nepal', 'Bhutan'],
            'Africa': ['Algeria', 'Angola', 'Benin'],
            'Europe': ['England', 'France', 'Germany', 'Spain'],
            'North America': ['USA', 'Canada', 'Mexico'],
            'South America': ['Brazil', 'Argentina', 'Peru'],
            'Australia': ['Australia', 'New Zealand']
        }
        populate_database(REGIONS)
        self.stdout.write('Successfully Populated Database')
