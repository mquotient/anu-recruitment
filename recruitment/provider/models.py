from django.db import models
from django.core.validators import RegexValidator
from django.core.exceptions import ValidationError
from django.contrib.postgres.fields import ArrayField


# ChainedForeignKey is used for achieving dependent dropdown.
# From an external library smart selects
# Source - https://github.com/digi604/django-smart-selects

from smart_selects.db_fields import ChainedForeignKey 


# A validation pattern for provider name using regex

VALID_PROVIDER_NAME_PATTERN = RegexValidator(r'^[a-zA-Z][a-zA-Z0-9_]*$')


# Function used for validating if the given file is of .txt format else
# raising validation error 

def validate_file_extension(value):
    try:
        content_type = value.file.content_type
    except:
        return
    if content_type != 'text/plain':
        raise ValidationError('File not supported!')


class Geography(models.Model):
    name = models.CharField(max_length=50)

    def __unicode__(self):
        return self.name


class Country(models.Model):
    name = models.CharField(max_length=50)
    geography = models.ForeignKey(Geography)

    def __unicode__(self):
        return self.name + '(' + self.geography.name + ')'


# Contains ArrayField, which is used for storing lists. Source -
# https://docs.djangoproject.com/en/1.11/ref/contrib/postgres/fields/

class Provider(models.Model):
    name = models.CharField(
        max_length=50, validators=[VALID_PROVIDER_NAME_PATTERN])
    geography = models.ForeignKey(Geography)
    country = ChainedForeignKey(Country,
        chained_field='geography',
        chained_model_field='geography',
        show_all=False,
        auto_choose=True,
        sort=True)
    account = ArrayField(
        models.CharField(max_length=50), size=50, null=True, blank=True)

    def __unicode__(self):
        return self.name


# Contains a FileField whose contents will be used to update the
# account attribute in the corresponding Provider class.
 
class Account(models.Model):
    name = models.CharField(max_length=50)
    provider = models.ForeignKey(Provider, related_name='accounts')
    accounts_file = models.FileField(
        upload_to='files/', validators=[validate_file_extension])

    def __unicode__(self):
        return self.name + '(' + self.provider.name + ')'
